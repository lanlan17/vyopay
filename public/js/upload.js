var server = location.protocol + '//' + location.host + '/vy_pay/';
let select_merchant = $('#merchant_select');


    $('.upload-btn').click(function(){
    var formData = new FormData();
    merchant_name = select_merchant.val();
   files =  $('#file-browser')[0].files[0];
   formData.append('file',files);
   // formData.append('merchant_id',merchant_name);
    
    $.ajax({
        url: server+'upload/start',
        type: 'POST',
        data: formData,
        beforeSend:function(){
            // unhide loading
            $('.modal').fadeIn();
        },
        error:function(x,y,z){
            // hide loading
            $('.modal').fadeOut();
            console.log(x);
            console.log(y);
            console.log(z);
        },
        success:function(res){
         $('#file-holder').val('');
         $('#file-browser').val(null);
            // hide loading
            $('.modal').fadeOut();
            alert(res.responseStr);
            location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });
})

$('.new-merchant-btn').click(function(){
    merchant_name = $('#text-merchant').val();
    $.ajax({
        url: server+'upload/addMerchant',
        type: 'POST',
        data:{'merchant_name':merchant_name},
        beforeSend:function(){
            $('.modal').fadeIn();
        },
        error:function(x,y,z){
            $('.modal').fadeOut();
            console.log(x);
            console.log(y);
            console.log(z);
        },
        success:function(res){
            $('.modal').fadeOut();
            alert(res.responseStr);
            reloadSelect();
            $('#text-merchant').val('');
        }
    });
});

$('.push-online-btn').click(function(){
    console.log('test');
    $.ajax({
        url: server+'upload/pushOnline',
        type: 'POST',
        beforeSend:function(){
            $('.modal').fadeIn();
        },
        error:function(x,y,z){
            $('.modal').fadeOut();
            console.log(x);
            console.log(y);
            console.log(z);
        },
        success:function(res){
            console.log(res);
            $('#divbugger').html(res.responseStr);
            alert(res.responseStr);
            $('.modal').fadeOut();
        }
    });
})

 function reloadSelect()
    {
        $.ajax({
        url: server+'upload/showMerchant',
        type: 'POST',
        // dataType:'json',
        beforeSend:function(){},
        error:function(x,y,z){
            console.log(x);
            console.log(y);
            console.log(z);
        },
        success:function(res){
           
           let option;
           option = '<option value="">Please Select Merchant</option>';
            $.each(res.data,function(a,i){
                option += '<option value="'+i.merchant_id+'">'+i.merchant_name+'</option>';
            });
            select_merchant.html(option);
        }
    });
    }


$(function(){
    reloadSelect();
  
})