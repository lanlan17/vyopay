<?php 
class Base64 
{

	public static function encrypt($data)
	{
	
		$array     = str_split($data,1);
		$encrypted = '';

		for($x = 0; $x <= sizeof($array) - 1; $x++)
		{
			$encrypted .= " " . decbin((ord($array[$x]) + 15));
		}
		$encrypted = base64_encode($encrypted);

		 return $encrypted;
	}

	public static function decrypt($data)
	{
		
		$decrypted = base64_decode($data);
		$array     = explode(' ',$decrypted);
		$decrypted = '';

		for($x = 1;$x <= sizeof($array) - 1; $x++)
		{
			$decrypted .= chr((bindec($array[$x])) - 15);
		}

		return $decrypted;
	}
}