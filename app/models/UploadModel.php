<?php 
// error_reporting(0);
ini_set('max_execution_time', 0);
class UploadModel 
{

	public function __construct()
	{
			$this->transactions = [];
		App::load_library('connection/pdo-connection');
		App::load_library('connection/procedural-connection');
		App::load_library('filter-data/filter-data');
		App::load_library('PHPExcel');
		$this->con = new PDO_connection(HOST,USER,PASS,DB_NAME);
		// $con_online = array(
		// 	'HOST'=>'166.62.101.81',
		// 	'USERNAME'=>'tempic4cadmin',
		// 	'PASSWORD'=>'admin1231A!',
		// 	'DB_NAME'=>'tempic4c'
		// );
		// $this->con_online = new PDO_connection($con_online['HOST'],$con_online['USERNAME'],$con_online['PASSWORD'],$con_online['DB_NAME']);
	}

	public function upload()
	{

		$file = $this->check_file();
		// if(empty($request['merchant_id']))
		// {
		// 	return array('response'=>0,'responseStr'=>'Please Select Merchant.');
		// }
		if(empty($file)){
			exit;
		}
		else{

			$ext = isset($file['ext']) ? $file['ext'] : '';
			$file = isset($file['tmp_file']) ? $file['tmp_file'] : '';
			$file_reader = (strtolower($ext) == 'csv') ? 'CSV' : 'Excel2007';

			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$excel = PHPExcel_IOFactory::createReader($file_reader);

			if($file){

				$excel = $excel->load($file);
				$excel->setActiveSheetIndex(0);
				$x = 0;
				$id = '';
				while ($id == "") {
					$x++;
					$id = $excel->getActiveSheet()->getCell('A'.$x)->getValue();
					if($id != "")
					{

						// $data = array('counter'=>$x,'excel'=>$excel,'merchant_info'=>$request);
						$data = array('counter'=>$x,'excel'=>$excel);

						$this->getData($data);

						break;
						
					}
				}
				
			}
			else{

				return array('responsecode'=>-1,'responseStr'=>'Invalid file type.');
			}
		}
		

	}

	public function getData($data)
	{

		$this->transactions = [];
		
		// $merchant_id = $data['merchant_info']['merchant_id'];
		$counter = $data['counter'] + 1;
		$excel = $data['excel'];

		$excel->setActiveSheetIndex(0);
		while ($excel->getActiveSheet()->getCell('B' . $counter)->getValue() != "") {
			$result = [
				'id' => $excel->getActiveSheet()->getCell('A' . $counter)->getValue(),
				'merchant' => $excel->getActiveSheet()->getCell('B' . $counter)->getValue(),
				'transdate' => $excel->getActiveSheet()->getCell('C' . $counter)->getValue(),
				'is_prepaid' => $excel->getActiveSheet()->getCell('D' . $counter)->getValue(),
				'trantype' => $excel->getActiveSheet()->getCell('E' . $counter)->getValue(),
				'mid_number' => $excel->getActiveSheet()->getCell('F' . $counter)->getValue(),
				'mid_name' => $excel->getActiveSheet()->getCell('G' . $counter)->getValue(),
				'auth_transaction_id' => $excel->getActiveSheet()->getCell('H' . $counter)->getValue(),
				'first_6' => $excel->getActiveSheet()->getCell('I' . $counter)->getValue(),
				'cardnum' => $excel->getActiveSheet()->getCell('J' . $counter)->getValue(),
				'cardfullname' => $excel->getActiveSheet()->getCell('K' . $counter)->getValue(),
				'amount1' => $excel->getActiveSheet()->getCell('L' . $counter)->getValue(),
				'amount' => $excel->getActiveSheet()->getCell('M' . $counter)->getValue(),
				'response_status' => $excel->getActiveSheet()->getCell('N' . $counter)->getValue(),
				'message' => $excel->getActiveSheet()->getCell('O' . $counter)->getValue(),
				'orderid' => $excel->getActiveSheet()->getCell('P' . $counter)->getValue(),
				'gentype' => $excel->getActiveSheet()->getCell('Q' . $counter)->getValue()
				// 'merchant_id' => $merchant_id

			];
			$counter++;
			array_push($this->transactions, $result);
			
		}
		// $this->update_data($result,'vy_transactions','id');
		// $this->update_data();
		$this->AddtoLocal();


	}

	// private function update_data($array_data,$table,$identifier)
	private function update_data()

	{

		$transacOnline = [];

		// exit(print_r($array_data));
		// exit(print_r($array_data));
		// $params = '';
		// if($table == 'vy_merchants')
		// {
		// 	$params = $array_data['merchant_id'];
		// }else
		// {
		// 	$params = $array_data['id'];
		// }

		// $columns = array_keys($array_data);

		// $values = [];
		// foreach ($columns as $key => $value) {
		// 	array_push($values, ':'.$value);
		// }
		// $query = "SELECT ".implode(",", $columns)." FROM $table WHERE $identifier = :parameter";
		// exit($query);
			$this->AddtoLocal($this->transactions);
		
		// exit(print_r($transacOnline));

	
		// if(!$data)
		// {
		// 	array_push($this->transactions, $array_data);
		// }
	}
	// private function insert_data($array_data,$con,$table)
	private function insert_data($array_data)
	{

		$transData = '';
		$transacOnline=[];

		$con = array(
			'HOST'=>'166.62.101.81',
			'USERNAME'=>'tempic4cadmin',
			'PASSWORD'=>'admin1231A!',
			'DB_NAME'=>'tempic4c'
		);

		$con_online = new PDO_connection($con['HOST'],$con['USERNAME'],$con['PASSWORD'],$con['DB_NAME']);

		// exit(print_r($columns));
		// $values = [];
		// foreach ($columns as $key => $value) {
		// 	array_push($values, ':'.$value);
		// }
		// exit(json_encode( array('response'=>0,'responseStr'=>'insert_data')));


		// foreach ($this->transactions as $transaction => $data) {
		// 	// exit(print_r($data));
	
		// 	$query = "SELECT * FROM vy_transactions WHERE id = :parameter";
		// 	$con_online->query($query);
		// 	$con_online->params('parameter',$data['id']);
		// 	$con_online->execute();
		// 	$result = $con_online->result();

		// 	if(!$result)
		// 	{
		// 		array_push($transacOnline, $data);
		// 	}
		// }
		// if(count($transacOnline) <= 0)
		// {
		// 	// exit('tets');
		// 	exit(json_encode( array('response'=>0,'responseStr'=>'No changes in online data.')));
		// }
		$transacOnline = $array_data;

		$this->columns = ['id','merchant','transdate','is_prepaid','trantype','mid_number','mid_name','auth_transaction_id','first_6','cardnum','cardfullname','amount1','amount','response_status','message','orderid','gentype'];
		foreach ($transacOnline as $key => $value) {
			$transData .= "('".$value['id']."'
							,'".$value['merchant']."'
							,'".$value['transdate']."'
							,'".$value['is_prepaid']."'
							,'".$value['trantype']."'
							,'".$value['mid_number']."'
							,'".$value['mid_name']."'
							,'".$value['auth_transaction_id']."'
							,'".$value['first_6']."'
							,'".$value['cardnum']."'
							,'".$value['cardfullname']."'
							,'".$value['amount1']."'
							,'".$value['amount']."'
							,'".$value['response_status']."'
							,'".$value['message']."'
							,'".$value['orderid']."'
							,'".$value['gentype']."'),";
		}
		$query = "INSERT IGNORE INTO vy_transactions(".implode(",", $this->columns).") VALUES".rtrim($transData,',');
// exit(print_r(implode(",", $this->columns),true));
// exit(json_encode( array('response'=>1,'responseStr'=>$query)));
		$con_online->query($query);
		$con_online->execute();
		$result = $con_online->result();

		$this->localHistoryInsert();

		exit(json_encode( array('response'=>1,'responseStr'=>'Successfully Upload To Online')));
	}

	public function AddtoLocal(){
		$array_data = $this->transactions;
		$transData = '';
		if (count($array_data)<=0) {
			exit(json_encode( array('response'=>1,'responseStr'=>'CSV Empty')));
		}

		$this->columns = ['id','merchant','transdate','is_prepaid','trantype','mid_number','mid_name','auth_transaction_id','first_6','cardnum','cardfullname','amount1','amount','response_status','message','orderid','gentype'];

		foreach ($array_data as $key => $value) {
			$transData .= "('".$value['id']."'
							,'".$value['merchant']."'
							,'".$value['transdate']."'
							,'".$value['is_prepaid']."'
							,'".$value['trantype']."'
							,'".$value['mid_number']."'
							,'".$value['mid_name']."'
							,'".$value['auth_transaction_id']."'
							,'".$value['first_6']."'
							,'".$value['cardnum']."'
							,'".$value['cardfullname']."'
							,'".$value['amount1']."'
							,'".$value['amount']."'
							,'".$value['response_status']."'
							,'".$value['message']."'
							,'".$value['orderid']."'
							,'".$value['gentype']."'),";
		}
		$query = "INSERT IGNORE INTO vy_transactions(".implode(",", $this->columns).") VALUES".rtrim($transData,',');

		$this->localInsert($query);
	}

	public function localInsert($query)
	{
		
		$this->con->query($query);
		$this->con->execute();

		exit(json_encode( array('response'=>1,'responseStr'=>'Successfully Added To Local')));
	}
	

	public function localHistoryInsert()
	{
		$query = "INSERT INTO logs_history(".implode(",", $this->columns ).") SELECT ".implode(",", $this->columns )." FROM vy_transactions WHERE id NOT IN(SELECT id FROM logs_history);";
		// exit(	$query);
		$this->con->query($query);
		$this->con->execute();
		$this->truncateTable();


		//pleasee add logs_history to local with same columns for local vy_transactions
	} 

	public function truncateTable()
	{
		$query = "TRUNCATE TABLE vy_transactions";
		$this->con->query($query);
		$this->con->execute();
	}

	private function check_file()
	{
		// exit('asd');
		if(isset($_FILES['file']))
		{
			$fileName = $_FILES['file']['name'];
	    	$fileExt = explode('.', $fileName);	
	    	$fileActualExt = strtolower(end($fileExt));
	    	$fileSize = $_FILES['file']['size'];
	    	$allowed = array('xls','xlsx','csv');
	    	$tmpFile = $_FILES['file']['tmp_name'];

	    	if(in_array($fileActualExt, $allowed))
	    	{

	    		return array('tmp_file'=>$tmpFile,'ext'=>$fileActualExt);
	    	}else
	    	{
	    		// exit('tets');
	    		return array('response'=>-1,'responseStr'=>'Invalid file type.');
	    		// exit();
	    	}
		}else
		{
			return array('response'=>-1,'responseStr'=>'Insert file.');
		}
	
	}

	public function addMerchant($request)
	{
		$merchant_name = isset($request['merchant_name']) ? $request['merchant_name'] : '';
		// exit($merchant_name);
		if(empty($merchant_name))
		{
			return array('response'=>-1,'responseStr'=>'Empty fields detected.!');
		}else
		{
			$query = "INSERT INTO vy_merchant(merchant_name)VALUES('{$merchant_name}')";
			$this->con->query($query);
			$this->con->execute();
			$result = $this->con->result();

			if($result){
				return array('response'=>1,'responseStr'=>'successfully added');
			}else
			{
				return array('response'=>0,'responseStr'=>'There was an error occured.');
			}
		}
		
		

	}

	public function showMerchant()
	{
		$query = "SELECT * FROM vy_merchant";
		$this->con->query($query);
		$this->con->execute();
		return $this->con->result();

	}

	public function showAllTransaction()
	{
		$query = "SELECT * FROM vy_transactions";
		$this->con->query($query);
		$this->con->execute();
		return  $this->con->result();
	}

	public function RecordCount()
	{
		$query = "SELECT count(*) as recordcount FROM vy_transactions";
		$this->con->query($query);
		$this->con->execute();
		$qresult = $this->con->result();
		$resultarr = ['recordcount'=>$qresult[0]['recordcount']];

		return  $resultarr;
	}

	public function pushToOnline()
	{
		$transactions = $this->showAllTransaction();
				// exit(json_encode(array('response'=>1,'responseStr'=>print_r($transactions,true))));
		

		if(count($transactions) <= 0)
		{
			return array('response'=>0,'responseStr'=>'No data from local detected.');
		}else
		{
			$this->insert_data($transactions);
		}
	}

	// public function pushOnline()
	// {
	// 	$con = array(
	// 		'HOST'=>'166.62.101.81',
	// 		'USERNAME'=>'tempic4cadmin',
	// 		'PASSWORD'=>'admin1231A!',
	// 		'DB_NAME'=>'tempic4c'
	// 	);

	// 	$transactions = $this->showAllTransaction();
	// 	$merchants = $this->showMerchant();

	// 	$this->online = new PDO_connection($con['HOST'],$con['USERNAME'],$con['PASSWORD'],$con['DB_NAME']);

	// 	foreach ($transactions as $key => $value) {
	// 			$this->update_data($transactions[$key],$this->online,$this->online2,'vy_transactions','id');
	// 	}
	// 	foreach ($merchants as $key => $value) {
	// 			$this->update_data($merchants[$key],$this->online,$this->online2,'vy_merchants','merchant_id');
	// 	}

	// 	return array('response'=>1,'responseStr'=>'online updating done.');
	// }
}