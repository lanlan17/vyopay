<?php 
date_default_timezone_set('Asia/Manila');
ini_set('max_execution_time', 0);

class Upload extends Controller
{
	public function index()
	{

		$data = ['recordcount'=> 0];
		$model_use = $this->model('UploadModel');
		$data = $model_use->RecordCount();

		$this->view('upload/index',$data);
		$this->view('templates/footer',array(JS_PATH.'jquery.js',JS_PATH.'upload.js',JS_PATH.'scripts.js'));
	
	}
	public function start()
	{
		header('Content-type: application/json');
		$model_use = $this->model('UploadModel');
		// exit(json_encode(array('response'=>0,'responseStr'=>'No changes in online data.'))
		$data = $model_use->upload();
		exit(json_encode($data));		
	}

	public function addMerchant()
	{
		header('Content-type: application/json');
		$model_use = $this->model('UploadModel');
		$data = $model_use->addMerchant($this->request);
		exit(json_encode($data));		
	}

	public function showAllTransaction()
	{
		header('Content-type: application/json');
		$model_use = $this->model('UploadModel');
		$data = $model_use->showAllTransaction($this->request);
		exit(json_encode($data));		
	}

	public function showMerchant()
	{
		header('Content-type: application/json');
		$model_use = $this->model('UploadModel');
		$data = $model_use->showMerchant();
		 $result = array('response'=>1,'data'=>$data);
		exit(json_encode($result));		
	}

	public function pushOnline()
	{
		header('Content-type: application/json');
		$model_use = $this->model('UploadModel');
		$data = $model_use->pushToOnline();
		exit(json_encode($data));		
	}
}