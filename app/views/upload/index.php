<!DOCTYPE html>
<html>

<head>
    <title>VYAPAY</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php  echo URL.'/public/css/styles.css'; ?>">
</head>

<body>

<div class="container flex-column">
    <div class="main-form flex-column">
<!--         <div class="flex adding-new-merchant actions-panel">
            <div class="flex-column panels">
                <label>Merchant</label>
                <div class="flex">
                    <input type="text" name="merchant_name" id="text-merchant">
                    <button class="new-merchant-btn">Add</button>
                </div>
            </div>
        </div> -->

        <div class="flex-column uploading-panel actions-panel">
  <!--           <div class="flex-column panels merchant-list-panel">
                <label>Select Merchant</label>
                <div class="flex">
                    <select  name="merchant_id" id="merchant_select">
                        <option value=""></option>
                    </select>
                    <button style="visibility:hidden"></button>
                </div>
            </div> -->
            <div class="flex-column panels merchant-list-panel">
                <label>No. of Current Local Data: <?=$recordcount; ?></label>
                <div style="display:none;" id="divbugger"></div>
            </div>
            <div class="flex-column">
                <label>Vyapay CSV File Uploader</label>
                <div class="flex panels file-upload-panel">
                    <input type="file" style="display:none" id="file-browser">
                    <input type="text" id="file-holder" name="file" readonly>
                    <button id="browse-btn" class="browse-btn">Browse</button>
                </div>

                <div class="flex panels btn-panel">
                    <div class="upload-btn-panel">
                        <a class="upload-btn action-btns">
                            <span>Local Upload</span>
                            <svg class="load" version="1.1"x="0px" y="0px" width="30px" height="30px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
                                <path opacity="0.3" fill="#fff" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
                                s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
                                c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
                                <path fill="#fff" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
                                C22.32,8.481,24.301,9.057,26.013,10.047z">
                                <animateTransform attributeType="xml"
                                attributeName="transform"
                                type="rotate"
                                from="0 20 20"
                                to="360 20 20"
                                dur="0.5s"
                                repeatCount="indefinite"/>
                                </path>
                            </svg>
                            <svg class="check" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"/>
                            </svg>
                        </a>
                        <div>
                            <span></span>
                        </div>
                    </div>
                    <div class="push-online-panel">
                        <button class="push-online-btn action-btns">Push Local to Online</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal">
    <div class="content-modal">
        <img src="<?php echo URL.'/public/images/loader.gif'; ?>?>" width="30"/>Loading please be patient..
    </div>
</div>
