<html>
	<head>
 			<link rel="stylesheet" href="<?php 	echo URL.'/public/css/bootstrap.min.css'; ?>">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <?php foreach($data as $key=>$value): ?>
        <link rel="stylesheet" href="<?= $value; ?>">
      <?php endforeach; ?>
	</head>

	<body class style>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo URL.'/home'; ?>">Green Box</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL.'/home'; ?>">Home <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL.'/upload'; ?>">Upload</a>
      </li>
      <li class="nav-item">
       <?php if(isset($_SESSION['user'])) : ?>
        <a class="nav-link" href="<?php echo URL.'/home/logout'; ?>">Logout</a>
        <?php else : ?>
          <a class="nav-link" href="<?php echo URL.'/login'; ?>">Login</a>
          <?php endif; ?>
      </li>
    </ul>
  </div>
</nav>
		<div class="container" style="margin-top: 30px;margin-bottom: 30px;">
		